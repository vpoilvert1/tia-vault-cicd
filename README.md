# tia-vault-cicd

## Description

This repository shows how you can integrate HashiCorp Vault with a Gitlab CI/CD.

It demonstrates the topics presented in my talk "Secure Your Multi-Cloud Delivery Pipeline with HashiCorp Vault" given at [DevFest Nantes (fr)](https://youtu.be/3eICrAQu1qo) and [HashiTalks Europe (en)](https://youtu.be/iUn7XINLlxA).

## Prerequisites

* terraform 1.1.7
* vault 1.9.4
* OpenSSH ssh client
* Ansible 2.9.6 (optional)
* An AWS account

## Directory structure

```
├── README.md
├── demo
│   └── backend    # Ansible code to deploy backend
└── setup
    ├── Dockerfile # Docker image used by Gitlab job 
    ├── Makefile   # Setup scripts
    ├── bin        # Utility shell scripts
    ├── infra      # Terraform code to deploy EC2 instance
    └── vault      # Terraform code to configure Vault server
```

## Installation

These commands must be run from your terminal with AWS credentials configured.

```bash
# deploy EC2 instance
cd setup/
rm -rf build/
make infra-deploy

# configure vault server
source bin/env.sh
make vault-unseal
source bin/env.sh
make vault-config

# deploy vault ssh ca to ec2 instance
make vault-deploy-ca

# manually configure VAULT_ADDR as environment variable in Gitlab
```

After the demo the environment can be cleaned with a few commands :
```bash
vault write -force /sys/leases/revoke-prefix/auth/approle/
vault write -force /sys/leases/revoke-prefix/aws-dev/
make vault-clean
make infra-clean
```

## Demo

### 1. Vault authentication

**Goal** : Use AppRole authentication method in the Gitlab job.

Show AppRole configuration :
```bash
# The role used by Gilab is created with three policies attached (see setup/vault/gitlab.tf)
vault list /auth/approle/role
vault read /auth/approle/role/tia-vault-cicd-dev

# The policies give limited permissions to respect the Principle of Least Privilege
vault read /auth/approle/role/tia-vault-cicd-dev
vault read /sys/policy/ssh-ubuntu-dev
vault read /sys/policy/aws-ec2-ro-dev
```

Configure `role_id` / `secret_id` in Gitlab :
```bash
# Get role_id and generate a new secret_id
vault read auth/approle/role/tia-vault-cicd-dev/role-id
vault write -force auth/approle/role/tia-vault-cicd-dev/secret-id

# Manually configure ROLE_ID and SECRET_ID as environment variables in Gitlab
# Ideally the secret_id should be rotated on a regular basis
```

Update `.gitlab-ci.yml` to use ROLE_ID and SECRET_ID variables to generate a Vault token :
```yaml
    - echo "Log in to Vault"
    - VAULT_TOKEN=$(vault write -field token auth/approle/login role_id=$ROLE_ID secret_id=$SECRET_ID)
    - export VAULT_TOKEN
    - vault token lookup
```

**Result** : The Gitlab job can generate a valid Vault token with limited permissions.

## 2. AWS authentication

**Goal** : Generate AWS short-lived credentials that will be used for Ansible dynamic inventory.

Show AWS secret backend configuration :
```bash
# See that AWS secret backend is setup (see setup/vault/main.tf)
vault secrets list
# Read AWS role configuration
vault read /aws-dev/roles/ec2-ro
```

Update `.gitlab-ci.yml` to generate AWS credentials with Vault :
```yaml
    - echo "Log in to AWS"
    - ASSUME_ROLE=$(vault write -format json aws-dev/sts/ec2-ro ttl=15m)
    - export AWS_ACCESS_KEY_ID=$(echo "${ASSUME_ROLE}" | jq -r '.data.access_key')
    - export AWS_SECRET_ACCESS_KEY=$(echo "${ASSUME_ROLE}" | jq -r '.data.secret_key')
    - export AWS_SESSION_TOKEN=$(echo "${ASSUME_ROLE}" | jq -r '.data.security_token')
```

**Result** : The Gitlab job can generate AWS credentials that expire with the Vault token.

## 3. Remote server authentication

**Goal** : Generate short-lived ssh-keys that can be used to connect to remote server.

Show SSH dynamic secrets backend configuration :
```bash
# See that SSH secret backend is setup (see setup/vault/main.tf)
vault secrets list
# Read SSH role configuration
vault read /ssh-client-signer-dev/roles/ubuntu
```

Update `.gitlab-ci.yml` to generate and sign SSH keys :
```yaml
    - echo "Generate and sign ssh key pair"
    - cd demo/backend/
    - ssh-keygen -t ed25519 -b 2048 -f id_ed25519 -N ''
    - vault write -field=signed_key ssh-client-signer-dev/sign/ubuntu public_key=@id_ed25519.pub > id_ed25519-signed.pub
    - chmod 600 id_ed25519-signed.pub
    - ssh-keygen -Lf id_ed25519-signed.pub
```

**Result** : The Gitlab job can use signed ssh keys to deploy the backend application with Ansible.

## 4. Static secrets

**Goal** : Consume a static secret stored in Vault to configure backend application.

Add a static secret to Vault :
```bash
# Add static secret
vault kv put secrets-dev/backend message="Hello Devoxx"
vault kv get secrets-dev/backend
```

**TODO** : Read static secret value in Ansible playbook.

**Result** : Ansible playbook can read static secrets from Vault.

## 5. Configure backend with Ansible

**Goal** : Execute Ansible playbook in Gitlab job with the secrets generated in the previous steps.

Update `.gitlab-ci.yml` to execute Ansible with AWS EC2 credentials and the signed ssh keys :
```yaml
    - echo "Deploy backend"
    - ansible-playbook -i inventory/ --ssh-common-args='-i id_ed25519-signed.pub -i id_ed25519 -o StrictHostKeyChecking=no' deploy.yml
```

**Result** : Ansible deploys the backend application using generated credentials.

## 6. Audit

**Goal** : Use Vault audit log to audit the pipeline actions.

Get `role_id` hash used to search audit log :
```bash
# Get gitlab requests
ROLE_ID=$(vault read -field role_id auth/approle/role/tia-vault-cicd-dev/role-id)
ROLE_ID_HASH=$(vault write -field hash /sys/audit-hash/file input=$ROLE_ID)
```

Search requests done with this `role_id` :
```bash
# Login to vault instance
ssh ubuntu@${INSTANCE_PUBLIC_IP}
sudo su
cd /opt/vault/log/
ll

# Show audit log
cat audit.log | jq | less

# Search for role_id api calls
jq ". | select(.request.data.role_id==\"${ROLE_ID_HASH}\")" audit.log | less
```

**Result** : We are able to list all Vault requests performed with the Gitlab pipeline `role_id`.

## 7. Revoke

**Goal** : Revoke all AWS credentials generated by Vault with a single command.

```bash
# List aws leases
vault list /sys/leases/lookup/aws-dev/sts/ec2-ro
# Revoke aws leases
vault write -force /sys/leases/revoke-prefix/aws-dev/sts/ec2-ro/
# List aws leases again
vault list /sys/leases/lookup/aws-dev/sts/ec2-ro
```

**Result** : All aws credentials are revoked.
