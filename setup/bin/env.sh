unset INSTANCE_PUBLIC_IP VAULT_ADDR VAULT_TOKEN

cd infra/
INSTANCE_PUBLIC_IP=$(terraform output -raw instance_public_ip)
VAULT_ADDR="http://${INSTANCE_PUBLIC_IP}:8200"
export INSTANCE_PUBLIC_IP VAULT_ADDR
cd ..

if [ -f "build/vault-init.json" ] ; then
    VAULT_TOKEN=$(jq -r '.root_token' build/vault-init.json)
    export VAULT_TOKEN
fi
