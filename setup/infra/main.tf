resource "aws_key_pair" "tia_vault_cicd" {
  key_name   = "tia-vault-cicd"
  public_key = var.public_key_pair
}
