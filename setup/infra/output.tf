output "instance_public_ip" {
  value = aws_eip.vault_server.public_ip
}