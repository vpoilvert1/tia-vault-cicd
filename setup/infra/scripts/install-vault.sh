#!/bin/bash

set -euo pipefail

# wait until instance is ready
sleep 60

# install dependencies
apt-get update
apt-get install -y ca-certificates curl gnupg lsb-release unzip jq

# install vault
curl -L "https://releases.hashicorp.com/vault/1.9.4/vault_1.9.4_linux_amd64.zip" -o /tmp/vault.zip
unzip /tmp/vault.zip -d /usr/local/bin/
chmod +x /usr/local/bin/vault
groupadd vault  || true
useradd vault -g vault || true
mkdir -p /opt/vault/data /opt/vault/log
chown vault:vault /opt/vault/data /opt/vault/log

# configure vault
cat > /opt/vault/vault.hcl <<EOF
listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = 1
}

storage "file" {
  path = "/opt/vault/data"
}
EOF

# configure vault service
cat > /etc/systemd/system/vault.service <<EOF
[Unit]
Description="HashiCorp Vault - A tool for managing secrets" Documentation=https://www.vaultproject.io/docs/
Requires=network-online.target
After=network-online.target ConditionFileNotEmpty=/etc/vault.d/vault.hcl StartLimitIntervalSec=60
StartLimitBurst=3

[Service]
User=vault
Group=vault
ProtectSystem=full
ProtectHome=read-only
PrivateTmp=yes
PrivateDevices=yes
SecureBits=keep-caps
AmbientCapabilities=CAP_IPC_LOCK
Capabilities=CAP_IPC_LOCK+ep
CapabilityBoundingSet=CAP_SYSLOG CAP_IPC_LOCK
NoNewPrivileges=yes
ExecStart=/usr/local/bin/vault server -config=/opt/vault/vault.hcl ExecReload=/bin/kill --signal HUP \$MAINPID 
KillMode=process 
KillSignal=SIGINT 
Restart=on-failure 
RestartSec=5
TimeoutStopSec=30
StartLimitInterval=60
StartLimitIntervalSec=60
StartLimitBurst=3
LimitNOFILE=65536
LimitMEMLOCK=infinity

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable vault.service
systemctl restart vault.service
