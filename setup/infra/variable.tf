variable "environment" {
  type    = string
  default = "dev"
}

variable "public_key_pair" {
  type = string
}

variable "private_key_path" {
  type = string
}

variable "image_id" {
  type    = string
  default = "ami-0ef38d2cfb7fd2d03"
}

variable "instance_type" {
  type    = string
  default = "t3.small"
}
