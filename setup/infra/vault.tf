resource "aws_instance" "vault" {
  ami           = var.image_id
  instance_type = "t3.small"
  key_name      = aws_key_pair.tia_vault_cicd.key_name

  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [aws_security_group.vault.id]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file(var.private_key_path)
    host        = self.public_ip
  }

  provisioner "file" {
    source      = "scripts/install-vault.sh"
    destination = "/tmp/install-vault.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/install-vault.sh",
      "sudo /tmp/install-vault.sh",
    ]
  }

  tags = {
    Name        = "tia-vault-cicd"
    Environment = var.environment
  }
}

resource "aws_security_group" "vault" {
  name   = "tia-vault-cicd"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8200
    to_port     = 8200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "tia-vault-cicd"
    Environment = var.environment
  }
}

resource "aws_eip_association" "vault_server" {
  instance_id   = aws_instance.vault.id
  allocation_id = aws_eip.vault_server.id
}
