data "vault_policy_document" "ssh_ubuntu" {
  rule {
    path         = "${vault_mount.ssh_client_signer.path}/sign/ubuntu"
    capabilities = ["update"]
    description  = "Allow ssh client key signing for ubuntu dev"
  }
}

data "vault_policy_document" "aws_ec2_ro" {
  rule {
    path         = "${vault_aws_secret_backend.aws.path}/sts/ec2-ro"
    capabilities = ["update"]
    description  = "Allow aws credentials generation for ec2-ro dev"
  }
}