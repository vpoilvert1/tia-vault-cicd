resource "vault_auth_backend" "approle" {
  type = "approle"
}

resource "vault_approle_auth_backend_role" "tia_vault_cicd" {
  backend   = vault_auth_backend.approle.path
  role_name = "tia-vault-cicd-${var.environment}"

  token_ttl     = 1800
  token_max_ttl = 3600

  token_policies = [
    "default",
    vault_policy.ssh_ubuntu.name,
    vault_policy.aws_ec2_ro.name,
  ]
}
