resource "aws_iam_user" "vault" {
  name = "vault-${var.environment}"
  path = "/${var.environment}/"

  tags = {
    Environment = var.environment
  }
}

resource "aws_iam_access_key" "vault" {
  user = aws_iam_user.vault.name
}

data "aws_iam_policy_document" "vault" {
  statement {
    sid = "1"

    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      aws_iam_role.ec2_ro.arn,
    ]
  }
}

resource "aws_iam_user_policy" "vault" {
  name = "vault-${var.environment}"
  user = aws_iam_user.vault.name

  policy = data.aws_iam_policy_document.vault.json
}

data "aws_iam_policy_document" "ec2_ro" {
  statement {
    sid = "AllowEc2Ro"

    actions = [
      "ec2:Describe*",
      "ec2:Get*",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role" "ec2_ro" {
  name = "ec2-ro-${var.environment}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          AWS = aws_iam_user.vault.arn
        }
        Action = "sts:AssumeRole"
      },
    ]
  })

  inline_policy {
    name   = "ec2-ro"
    policy = data.aws_iam_policy_document.ec2_ro.json
  }

  tags = {
    Environment = var.environment
  }
}
