resource "vault_audit" "local_file" {
  type        = "file"
  description = "Write audit log to local file"

  options = {
    file_path = "/opt/vault/log/audit.log"
  }
}

resource "vault_mount" "secrets" {
  path        = "secrets-${var.environment}"
  type        = "generic"
  description = "Store key/value secrets in ${var.environment}"
}

resource "vault_mount" "ssh_client_signer" {
  path        = "ssh-client-signer-${var.environment}"
  type        = "ssh"
  description = "Sign ssh keys to connect to ${var.environment} servers"
}

resource "vault_ssh_secret_backend_ca" "ssh_client_signer" {
  backend              = vault_mount.ssh_client_signer.path
  generate_signing_key = true
}

resource "vault_ssh_secret_backend_role" "ssh_ubuntu" {
  name                    = "ubuntu"
  backend                 = vault_mount.ssh_client_signer.path
  key_type                = "ca"
  default_user            = "ubuntu"
  allowed_users           = "ubuntu"
  allow_user_certificates = true
  allowed_extensions      = "permit-pty,permit-port-forwarding"
  default_extensions = {
    "permit-pty" : ""
  }
  algorithm_signer = "rsa-sha2-512"
  ttl              = "3600"
}

resource "vault_policy" "ssh_ubuntu" {
  name = "ssh-ubuntu-${var.environment}"

  policy = data.vault_policy_document.ssh_ubuntu.hcl
}

resource "vault_aws_secret_backend" "aws" {
  access_key  = aws_iam_access_key.vault.id
  secret_key  = aws_iam_access_key.vault.secret
  region      = "eu-west-1"
  path        = "aws-${var.environment}"
  description = "Generate AWS credentials in ${var.environment}"
}

resource "vault_aws_secret_backend_role" "ec2_ro" {
  backend         = vault_aws_secret_backend.aws.path
  name            = "ec2-ro"
  credential_type = "assumed_role"
  role_arns       = [aws_iam_role.ec2_ro.arn]
  default_sts_ttl = 900
}

resource "vault_policy" "aws_ec2_ro" {
  name = "aws-ec2-ro-${var.environment}"

  policy = data.vault_policy_document.aws_ec2_ro.hcl
}
